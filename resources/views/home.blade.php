@extends('common.layout')
@section('title','HOME')
@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1" class=""></li>
        <li data-target="#demo" data-slide-to="2" class=""></li>
    </ul>

    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="http://sites.mobotics.in/eeaa/assets/images/home-bg.jpg" alt="Los Angeles" width="100%" />
            <div class="carousel-caption">
                <div class="container">
                    <div class="col-md-6">
                        <h1 class="">ALL-IN-ONE ONLINE SCHEDULING SOFTWARE</h1>
                        <p class="mt-3 mb-4">Trusted by 130,000+ customers worldwide<div class="clearfix"></div></p>
                        <a href="javascript:;" class="tri-btn btn btn-outline-light mr-3"> Button 1 </a>
                        <a href="javascript:;" class="tri-btn btn btn-info">Button 2</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img src="http://sites.mobotics.in/eeaa/assets/images/home-bg.jpg" alt="Los Angeles" width="100%" />
            <div class="carousel-caption">
                <div class="container">
                    <div class="col-md-6">
                        <h1 class="">ALL-IN-ONE ONLINE SCHEDULING SOFTWARE</h1>
                        <p class="mt-3 mb-4">Trusted by 130,000+ customers worldwide<div class="clearfix"></div></p>
                        <a href="javascript:;" class="tri-btn btn btn-outline-light mr-3"> Button 1 </a>
                        <a href="javascript:;" class="tri-btn btn btn-info">Button 2</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img src="http://sites.mobotics.in/eeaa/assets/images/home-bg.jpg" alt="Los Angeles" width="100%" />
            <div class="carousel-caption">
                <div class="container">
                    <div class="col-md-6">
                        <h1 class="">ALL-IN-ONE ONLINE SCHEDULING SOFTWARE</h1>
                        <p class="mt-3 mb-4">Trusted by 130,000+ customers worldwide<div class="clearfix"></div></p>
                        <a href="javascript:;" class="tri-btn btn btn-outline-light mr-3"> Button 1 </a>
                        <a href="javascript:;" class="tri-btn btn btn-info">Button 2</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>
<div class="main-video mt-5 mb-5">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h2>Welcome to EEAA</h2>
        <p class="mt-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
        <a class="btn btn-info tri-btn mt-4" href="javascript:;">Read More</a></div>
        <div class="col-md-5">
          <div class="video"><img src="{{ asset('assets/images/background/login-register.jpg') }}" width="100%" /> <a href="#"><i class="fas fa-play"></i></a></div>
      </div>
  </div>
</div>
</div>
<div class="timer-main pt-5 pb-5">
    <div class="container">
        <h2 class="text-center">Gallery</h2>
        <div class="head-line mb-5">
            <i class="fab fa-fly"></i>
        </div>
        <div class="col-md-12">
            <div class="best-staff">
                <div>
                     <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" width="100%" alt=""></a>
                </div>
                <div>
                     <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" width="100%" alt=""></a>
                </div>
                <div>
                     <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" width="100%" alt=""></a>
                </div>
                <div>
                     <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" width="100%" alt=""></a>
                </div>
                <div>
                     <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" width="100%" alt=""></a>
                </div>
                <div>
                     <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" width="100%" alt=""></a>
                </div>
            </div>
        <div class="best-staff-nav">
            <div class="best-staff-left"><i class="fa fa-angle-left"></i></div>
            <div class="best-staff-right"><i class="fa fa-angle-right"></i></div>
        </div>
  </div>
</div>
<div class="main-video mt-5 mb-4 pt-4 pb-4" style="background-color: #fff">
    <div class="container">
        <h2 class="text-center">News & Events</h2>
        <div class="head-line mb-5" style="background-color: #fff">
            <i class="fab fa-fly"></i>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body text-center">
                    {{-- <i class="fas fa-user"></i> --}}
                    <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" alt=""></a>

                    <a href="#" title="News & Events"><h3 class="text-center mt-3">Lorem ipsum dolor sit amet</h3></a>
                    <div class="box b-t text-center">
                         <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris.
                        </p>
                        <a href="#" title="News & Events" class="btn btn-info">Read More</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body text-center">
                    <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" alt=""></a>
                    <a href="#" title="News & Events"><h3 class="text-center mt-3">Lorem ipsum dolor sit amet</h3></a>
                    <div class="box b-t text-center">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris.
                        </p>
                        <a href="#" title="News & Events" class="btn btn-info">Read More</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body text-center">
                    <a href="#" title="News & Events"><img src="http://sites.mobotics.in/eeaa/assets/images/background/login-register.jpg" class="img-thumbnail" alt=""></a>
                    <a href="#" title="News & Events"><h3 class="text-center mt-3">Lorem ipsum dolor sit amet</h3></a>
                    <div class="box b-t text-center">
                         <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris.
                        </p>
                        <a href="#" title="News & Events" class="btn btn-info">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="clients Subscrib pb-5 mb-2">
        <div class="container">
            <h2 class="section-title bold600 text-center mb-3">Get Our Latest News Delivered Right to You</h2>
            <form method="post" action="" id="newsletter-form">
                <input type="hidden" name="_token" value="">                
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3  booking-form-item name">
                        <input class="form-control"  type="text" name="name" placeholder="Your name">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3  booking-form-item phone">
                        <input class="form-control"   type="tel" name="phone" placeholder="Phone">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3  booking-form-item email">
                        <input class="form-control"   type="text" name="email" placeholder="E-mail">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <button type="submit" class="btn btn-primary btn-block">Subscribe</button>
                        <div id="alert" style="display: none;"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection