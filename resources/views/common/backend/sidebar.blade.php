<?php
    $sess_user = Auth::user();
?>
<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="profile-img"> <img src="{{ asset('assets/images/dummy-avatar.png') }}" alt="user" /> </div>
            <div class="profile-text">
            <a href="{{ url('my-company') }}" aria-haspopup="true" aria-expanded="true">
                    <?php
                        echo ($sess_user->name != null) ? $sess_user->name : 'N/A';
                    ?>
                </a>
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">PERSONAL</li>
                <li class="active"> <a class="waves-effect waves-dark" href="{{ url('my-company') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                </li>

            <li class=""> <a class="waves-effect waves-dark" href="{{ url('employees') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Employees</span></a>
                </li>
                <li class=""> <a class="waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false"><i class="mdi mdi-package-variant"></i><span class="hide-menu">Permits</span></a>
                </li>
                <li class=""> <a class="waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false"><i class="mdi mdi-apps"></i><span class="hide-menu">Units</span></a>
                </li>
                <li class=""> <a class="waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false"><i class="mdi mdi-image-area"></i><span class="hide-menu">Protected Area</span></a>
                </li>
                <li class=""> <a class="waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false"><i class="mdi mdi-source-branch"></i><span class="hide-menu">Branches</span></a>
                </li>
                <li class=""> <a class="waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false"><i class="mdi mdi-ticket"></i><span class="hide-menu">Tickets</span></a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="sidebar-footer">
        <a href="#" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <a href="#" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
        <a href="#" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
</aside>