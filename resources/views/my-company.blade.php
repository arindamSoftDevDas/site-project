@extends('common.backend.layout')
@section('title', 'Dashboard')
@section('product_title', config('app.name'))
@section('content')
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 col-12 align-self-center">
                    <h3 class="text-white">Dashboard</h3>
                    <ol class="breadcrumb ">
                        <li class="breadcrumb-item"><a class="text-white" href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active text-white">Dashboard</li>
                    </ol>
                </div>
                <div class="col-md-7 col-4 align-self-center">
                </div>
            </div>
            <div class="row mt-low">

                <div class="col-lg-3 col-md-6">
                    <div class="card ">
                        <div class="card-body">
                            <h1 class=" text-center">23</h1>
                            <div class="col-12">
                                <div>
                                    <h3 class="card-title text-center text-uppercase">Employees</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h1 class=" text-center">34</h1>
                            <div class="col-12">
                                <div>
                                    <h3 class="card-title text-center text-uppercase">Permits</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h1 class=" text-center">100</h1>
                            <div class="col-12">
                                <div>
                                    <h3 class="card-title text-center text-uppercase">Branches</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h1 class=" text-center">20</h1>
                            <div class="col-12">
                                <div>
                                    <h3 class="card-title text-center text-uppercase">Tickets</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row ">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Monthly Report</h4>
                            <div id="bar-chart" style="width:100%; height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                © 2019 Admin
            </footer>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('assets/plugins/d3/d3.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/c3-master/c3.min.js') }}"></script>
    <script src="{{ asset('assets/js/dashboard1.js') }}"></script>
    <script src="{{ asset('assets/plugins/echarts/echarts-all.js') }}"></script>
    <script src="{{ asset('assets/plugins/echarts/echarts-init.js') }}"></script>
@endpush
        