<?php
namespace App\Http\Controllers;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {
	public function __invoke(Request $request) {
        $guard = 'org';
        if (Auth::guard($guard)->check()) {
			return redirect(route('home'));
        }
        
        if ($request->isMethod('post')) {
            $user = Organization::where(['email' => $request->email])->first();              
            if($user) {
                $credentials = [
                    'email' => $request->email,
                    'password' => $request->password
                ];
                $remember = $request->remember === 'on' ? true : false;
                if(Auth::guard($guard)->attempt($credentials, $remember)) {
                    // session(['session_id' => $sessions->first()->id]);
                    return ['type' => 'success', 'text' => 'Login Successfull'];
                }
                else {
                    return [ 'type' => 'error', 'text' => 'Invalid Username or Password'];
                }

            }
            else {
                return [ 'type' => 'error', 'text' => 'Invalid Username or Password'];
            }
        }
        else {
            return view('sign-in');
        }
    }
}