<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('home');
});

Route::get('/about-us', function () {
	return view('about-us');
});

Route::get('/sign-up', function () {
	return view('sign-up');
});


// Route::get('/my-companyd', function () {
// 	return view('my-company');
// });



Route::get('/thank-you', function () {
	return view('thank-you');
});

Route::get('/hash-pwd', function() {
	dd(Hash::make('password'));
});


Route::get('/clear-cache', function () {
	Artisan::call('cache:clear');
	return "Cache is cleared";
});


Route::get('/sign-in', function () {
	return redirect('sign-in');
});

Route::match(['get', 'post'], 'login', 'LoginController')->name('login')->middleware('web');
Route::match(['get'], 'sign-in', 'LoginController')->name('sign-in')->middleware('web');
Route::middleware(['auth:web,org'])->group(function () {
	Route::get('/my-company', 'HomeController@index')->name('home');


	// Route::get('/employees', function () {
	// 	return view('employees');
	// });
	
	Route::post('logout', function () {
		Auth::logout();
		return redirect(route('sign-in'));
	})->name('logout');

	Route::post('employees/change_status','EmployeeController@change_status')->name('employees.change_status');

	Route::group(['prefix' => 'profile'], function () {
		Route::get('/', 'ProfileController@index')->name('profile.index');
		Route::put('/', 'ProfileController@updatePersonal');
		Route::patch('/', 'ProfileController@updatePassword');
	});



	Route::resources([
		'employees' => 'EmployeeController'
	]);
});


